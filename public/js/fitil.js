/**
 * Created by nomce on 22/06/15.
 */

 //on start
 var jsonExport; // = new Object();
 var nbPlayers = 4;
 var nbGameGlobal = 0;

$(document).ready(function($) {


  var user=getCookie("fitilData");
  if ( user != '' ){
    var jsonImport;
        jsonImport = JSON.parse( user );
        var i, j;
        for (i = 1; i <= nbPlayers; i++) {
            $('#input-0-' + i).val(jsonImport.iminja[i]);
        }
        $('#dugmeStart').click();

        for (i = 1; i <= jsonImport.nbGameGlobal; i++) {
          for (j = 1; j <= nbPlayers; j++) {
              $('#input-' + i + '-' + j).val(jsonImport.igra[i][j]);
          }
          $('#btn-win').click();
        }

  }
});

$('#dugmeStart').click(function() {

  jsonExport = new Object();
  jsonExport.iminja = [];
  jsonExport.igra = [];

  $('#dugminja').removeClass("hidden");
  $('#X-1').removeClass("hidden");
  var ime;
  for (i = 1; i <= nbPlayers; i++) {
      $('#1-' + i).removeClass("hidden");
      ime = $('#input-0-' + i).val();
      jsonExport.iminja[i] = ime;
      $('#0-' + i).html( ime );
  }
  nbGameGlobal++;
  $( 'tbody' ).append( generateRow(nbGameGlobal) );
  $('#dugmeStart').addClass('hidden');
  $('#naslov').append(' (<span id="brojPartii">' + nbGameGlobal + '</span>)');
});

$('#btn-win').click(function() {
  var suma1, suma2, suma3, suma4;
  jsonExport.igra[nbGameGlobal] = [];
    suma1 = parseInt($('#input-' + nbGameGlobal + '-1' ).val(),10);
    if (isNaN(suma1)){ suma1 = 0;}
    suma2 = parseInt($('#input-' + nbGameGlobal + '-2' ).val(),10);
    if (isNaN(suma2)){ suma2 = 0;}
    suma3 = parseInt($('#input-' + nbGameGlobal + '-3' ).val(),10);
    if (isNaN(suma3)){ suma3 = 0;}
    suma4 = parseInt($('#input-' + nbGameGlobal + '-4' ).val(),10);
    if (isNaN(suma4)){ suma4 = 0;}

    if (suma1 + suma2 + suma3 + suma4 != 26){
      alert("Грешка во резултатите, збирот на сите поени не е 26!");
    }else{
        if (suma1 == 26 || suma2 == 26 || suma3 == 26 || suma4 == 26){
            if (suma1 == 26){
                suma1 = 0;
                suma2 = 26;
                suma3 = 26;
                suma4 = 26;
            }else if (suma2 == 26){
                suma1 = 26;
                suma2 = 0;
                suma3 = 26;
                suma4 = 26;
            }else if (suma3 == 26){
                suma1 = 26;
                suma2 = 26;
                suma3 = 0;
                suma4 = 26;
            }else{
                suma1 = 26;
                suma2 = 26;
                suma3 = 26;
                suma4 = 0;
            }
        }
      jsonExport.igra[nbGameGlobal][1] = parseInt(suma1,10);
      jsonExport.igra[nbGameGlobal][2] = parseInt(suma2,10);
      jsonExport.igra[nbGameGlobal][3] = parseInt(suma3,10);
      jsonExport.igra[nbGameGlobal][4] = parseInt(suma4,10);
      jsonExport.nbGameGlobal = nbGameGlobal;
      if (nbGameGlobal == 1){
          $('#' + nbGameGlobal + '-1').html(suma1);
          $('#' + nbGameGlobal + '-2').html(suma2);
          $('#' + nbGameGlobal + '-3').html(suma3);
          $('#' + nbGameGlobal + '-4').html(suma4);
      }else{
        var nbGameLocal = nbGameGlobal - 1;
        $('#' + nbGameGlobal + '-1').html(suma1 + parseInt($('#' + nbGameLocal + '-1').html(),10));
        $('#' + nbGameGlobal + '-2').html(suma2 + parseInt($('#' + nbGameLocal + '-2').html(),10));
        $('#' + nbGameGlobal + '-3').html(suma3 + parseInt($('#' + nbGameLocal + '-3').html(),10));
        $('#' + nbGameGlobal + '-4').html(suma4 + parseInt($('#' + nbGameLocal + '-4').html(),10));
      }
      if ($('#' + nbGameGlobal + '-1').html() > 100 || $('#' + nbGameGlobal + '-2').html() > 100 || $('#' + nbGameGlobal + '-3').html() > 100 || $('#' + nbGameGlobal + '-4').html() > 100){
        $( 'tbody' ).append( generateWin() );
      }else{
        $( 'tbody' ).append( generateRow(nbGameGlobal+1) );
      }
      setCookie("fitilData", JSON.stringify( jsonExport ), 365);
      colorateIt(nbGameGlobal);
      nbGameGlobal++;
      $('#brojPartii').html(nbGameGlobal);
    }
});

$('#btn-undo').click(function() {
  if (nbGameGlobal>1){
      var local = nbGameGlobal;
      nbGameGlobal--;
      $("#" + nbGameGlobal).remove();
      $("tbody tr:last-child").attr("id",""+nbGameGlobal);
      var player = 1;
      $("tbody tr:last-child > td").each(function () {
        $( this ).attr("id",""+ nbGameGlobal + "-" + player);
          $( this ).children('input').each(function () {
              $( this ).attr("id","input-"+ nbGameGlobal + "-" + player);
          });
        player++;
      });
      delete jsonExport.igra[nbGameGlobal];
      setCookie("fitilData", JSON.stringify( jsonExport ), 365);
      $('#brojPartii').html(nbGameGlobal);
  }
});

$('#btn-reset').click(function() {
  setCookie("fitilData", '', 365);
  window.location.reload(true);
});

function colorateIt(nbGame){
  var min, max, minI, maxI;
  min = parseInt($('#' + nbGame + '-1').html(),10);
  max = min;
  minI = 1;
  maxI = minI;
  for (i = 2; i <= nbPlayers; i++) {
    if ($('#' + nbGame + '-' + i).html() > max){
      max = parseInt($('#' + nbGame + '-' + i).html());
      maxI = i;
    }else if ($('#' + nbGame + '-' + i).html() < min){
      min = parseInt($('#' + nbGame + '-' + i).html(),10);
      minI = i;
    }
  }
  $('#' + nbGame + '-' + maxI).addClass("red");
  $('#' + nbGame + '-' + minI).addClass("green");
}

function generateRow(nbGame){
  var bla;
  bla = '<tr id="' + nbGame + '">\n';
  var i;
  for (i = 1; i <= nbPlayers; i++) {
    bla = bla + '<td id="' + nbGame + '-' + i + '" ><input id="input-'+ nbGame + '-' + i + '" class="form-control" type="number" /></td>\n';
  }
  bla = bla + '</tr>';
  return bla;
}

function generateWin(){
  var bla;
    var min, pobedi;
    min = $('#' + nbGameGlobal + '-1').html();
    pobedi = 1;
    for (var i = 2; i <= nbPlayers; i++) {
        if ($('#' + nbGame + '-' + i).html() < min){
            min = parseInt($('#' + nbGame + '-' + i).html(),10);
            pobedi = i;
        }
    }

  bla = '<tr><td colspan="4" >'+ jsonExport.iminja[pobedi] +' победи. Честито!</td></tr>';
  return bla;
}
