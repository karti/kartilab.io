/**
* Created by nomce on 22/06/15.
*/

//on start
var jsonExport // = new Object();
var nbPlayers;
var nbGameGlobal = 0;

$(document).ready(function($) {
  var user;
  user=getCookie("kartiData");
  if ( user != '' ){
    var jsonImport;
    jsonImport = JSON.parse( user );
    $('#players-bar-top').val( jsonImport.nbPlayers );
    $('#players-bar-top-button').click();
    var i, j;
    for (i = 1; i <= nbPlayers; i++) {
      $('#input-0-' + i).val(jsonImport.iminja[i]);
    }
    $('#dugmeStart').click();

    for (i = 1; i <= jsonImport.nbGameGlobal; i++) {
      for (j = 1; j <= nbPlayers; j++) {
        $('#input-' + i + '-' + j).val(jsonImport.igra[i][j][1]);
        if( jsonImport.igra[i][j][0] ){
          $('#check-' + i + '-' + j).attr('checked', 'checked');
        }
      }
      if (jsonImport.igra[i][1][2]){
        $('#btn-xwin').click();
      }else if(!jsonImport.igra[i][1][2]){
        $('#btn-win').click();
      }
    }
  }
});

$('#players-bar-top').keypress(function (e) {
  if (e.which == 13) {
    $('#players-bar-top-button').click();
    return false;
  }
});

$('#players-bar-top-button').click(function() {
  nbPlayers = $('#players-bar-top').val();
  initiateGame();
});
$('#2-players-bar-top-button').click(function() {
  nbPlayers = 2;
  initiateGame();
});
$('#3-players-bar-top-button').click(function() {
  nbPlayers = 3;
  initiateGame();
});
$('#4-players-bar-top-button').click(function() {
  nbPlayers = 4
  initiateGame();
});
$('#5-players-bar-top-button').click(function() {
  nbPlayers = 5;
  initiateGame();
});
$('#6-players-bar-top-button').click(function() {
  nbPlayers = 6;
  initiateGame();
});
$('#7-players-bar-top-button').click(function() {
  nbPlayers = 7;
  initiateGame();
});

function initiateGame() {
  jsonExport = new Object();
  var i;
  if(nbPlayers > 1) {
    $( '#iminja' ).append( generateNames() );
    //for (i = 1; i <= nbPlayers; i++) {
    //    $('#0-' + i).removeClass("hidden");
    //}
    $('#nb-players').addClass("hidden");
    $('.row').removeClass("hidden");
    jsonExport.nbPlayers = nbPlayers;
    jsonExport.iminja = [];
    jsonExport.igra = [];
  }else{
    alert("Неможеш сам да играш :)");
  }
}

$('#dugmeStart').click(function() {
  $('#1').removeClass("hidden");
  $('#dugminja').removeClass("hidden");
  $('#X-1').removeClass("hidden");
  $('#X-1').attr("colspan",nbPlayers);
  var ime;
  for (i = 1; i <= nbPlayers; i++) {
    $('#1-' + i).removeClass("hidden");
    ime = $('#input-0-' + i).val();
    jsonExport.iminja[i] = ime;
    $('#0-' + i).html( ime );
  }
  nbGameGlobal++;
  $( 'tbody' ).append( generateRow(nbGameGlobal) );
  $('#dugmeStart').addClass('hidden');
  $('#naslov').append(' (<span id="brojPartii">' + nbGameGlobal + '</span>)');
});

$('#btn-win').click(function() {
  var player = document.querySelector('input[name="pobeda"]:checked').value;
  var suma;
  var nbGameLocal;
  jsonExport.igra[nbGameGlobal] = [];
  for (i = 1; i <= nbPlayers; i++) {
    suma = 0;
    jsonExport.igra[nbGameGlobal][i] = [];
    jsonExport.igra[nbGameGlobal][i][0] = false;
    if (nbGameGlobal == 1){
      if (player == i){
        suma = -50;
        jsonExport.igra[nbGameGlobal][i][0] = true;
        jsonExport.igra[nbGameGlobal][i][1] = parseInt(suma,10);
      }else{
        suma = parseInt($('#input-' + nbGameGlobal + '-' + i).val(),10),
        jsonExport.igra[nbGameGlobal][i][1] = parseInt(suma,10);
        if ( ! suma ){
          suma = 100;
          jsonExport.igra[nbGameGlobal][i][1] = '';
        }
      }
    }else{
      nbGameLocal = nbGameGlobal-1;
      if (player == i){
        suma = $('#' + nbGameLocal + '-' + i).html();
        jsonExport.igra[nbGameGlobal][i][1] = parseInt('-50',10);
        suma = parseInt(suma,10) - 50;
        jsonExport.igra[nbGameGlobal][i][0] = true;
      }else{
        suma = $('#' + nbGameLocal + '-' + i).html();
        var suma2 = $('#input-' + nbGameGlobal + '-' + i).val();
        jsonExport.igra[nbGameGlobal][i][1] = parseInt(suma2,10);
        if ( ! suma2 ){
          suma2 = 100;
          jsonExport.igra[nbGameGlobal][i][1] = '';
        }
        suma = parseInt(suma,10) + parseInt(suma2,10);
      }
    }
    jsonExport.igra[nbGameGlobal][i][2] = false;
    jsonExport.nbGameGlobal = nbGameGlobal;
    setCookie("kartiData", JSON.stringify( jsonExport ), 365);
    $('#' + nbGameGlobal + '-' + i).html(suma);
  }
  colorateIt(nbGameGlobal);
  nbGameGlobal++;
  $('#brojPartii').html(nbGameGlobal);
  $( 'tbody' ).append( generateRow(nbGameGlobal) );
});

$('#btn-xwin').click(function() {
  var player = document.querySelector('input[name="pobeda"]:checked').value;
  var suma;
  var nbGameLocal;
  jsonExport.igra[nbGameGlobal] = [];
  for (i = 1; i <= nbPlayers; i++) {
    suma = 0;
    jsonExport.igra[nbGameGlobal][i] = [];
    jsonExport.igra[nbGameGlobal][i][0] = false;
    if (nbGameGlobal == 1){
      if (player == i){
        suma = -100;
        jsonExport.igra[nbGameGlobal][i][0] = true;
        jsonExport.igra[nbGameGlobal][i][1] = parseInt(suma,10);
      }else{
        suma = parseInt($('#input-' + nbGameGlobal + '-' + i).val(),10) *2;
        jsonExport.igra[nbGameGlobal][i][1] = parseInt($('#input-' + nbGameGlobal + '-' + i).val(),10);
        if ( ! suma ){
          suma = 200;
          jsonExport.igra[nbGameGlobal][i][1] = '';
        }
      }
    }else{
      nbGameLocal = nbGameGlobal-1;
      if (player == i){
        suma = $('#' + nbGameLocal + '-' + i).html();
        suma = suma - 100;
        jsonExport.igra[nbGameGlobal][i][1] = '-50';
        jsonExport.igra[nbGameGlobal][i][0] = true;
      }else{
        suma = $('#' + nbGameLocal + '-' + i).html();
        var suma2 = $('#input-' + nbGameGlobal + '-' + i).val();
        jsonExport.igra[nbGameGlobal][i][1] = parseInt(suma2,10);
        if ( ! suma2 ){
          suma2 = 100;
          jsonExport.igra[nbGameGlobal][i][1] = '';
        }
        suma = parseInt(suma,10) + (parseInt(suma2,10)*2);
      }
    }
    jsonExport.igra[nbGameGlobal][i][2] = true;
    jsonExport.nbGameGlobal = nbGameGlobal;
    setCookie("kartiData", JSON.stringify( jsonExport ), 365);
    $('#' + nbGameGlobal + '-' + i).html(suma);
  }
  colorateIt(nbGameGlobal);
  nbGameGlobal++;
  $( 'tbody' ).append( generateRow(nbGameGlobal) );
  $('#brojPartii').html(nbGameGlobal);
});

$('#btn-undo').click(function() {
  if (nbGameGlobal>1){
    var local = nbGameGlobal;
    nbGameGlobal--;
    $("#" + nbGameGlobal).remove();
    $("tbody tr:last-child").attr("id",""+nbGameGlobal);
    var player = 1;
    $("tbody tr:last-child > td").each(function () {
      $( this ).attr("id",""+ nbGameGlobal + "-" + player);
      $( this ).children('input').each(function () {
        if ( $( this ).is(':radio') ){
          $( this ).attr("id","check-"+ nbGameGlobal + "-" + player);
        }else{
          $( this ).attr("id","input-"+ nbGameGlobal + "-" + player);
        }
      });
      player++;
    });
    delete jsonExport.igra[nbGameGlobal];
    setCookie("kartiData", JSON.stringify( jsonExport ), 365);
    $('#brojPartii').html(nbGameGlobal);
  }
});

$('#btn-reset').click(function() {
  setCookie("kartiData", '', 365);
  window.location.reload(true);
});

function colorateIt(nbGame){
  var min, max, minI, maxI;
  min = parseInt($('#' + nbGame + '-1').html(),10);
  max = min;
  minI = 1;
  maxI = minI;
  for (i = 2; i <= nbPlayers; i++) {
    if ($('#' + nbGame + '-' + i).html() > max){
      max = parseInt($('#' + nbGame + '-' + i).html());
      maxI = i;
    }else if ($('#' + nbGame + '-' + i).html() < min){
      min = parseInt($('#' + nbGame + '-' + i).html(),10);
      minI = i;
    }
  }
  $('#' + nbGame + '-' + minI).addClass("green");
  $('#' + nbGame + '-' + maxI).addClass("red");
}

function generateNames(){
  var bla;
  bla = '';
  var i;
  for (i = 1; i <= nbPlayers; i++) {
    bla = bla + '<td id="0-' + i + '" ><input id="input-0-' + i + '" class="form-control" placeholder="Име" type="text"><span class="hidden"></span></td>';
  }
  bla = bla + '</tr>';
  return bla;
}


function generateRow(nbGame){
  var bla;
  bla = '<tr id="' + nbGame + '">\n';
  var i;
  for (i = 1; i <= nbPlayers; i++) {
    bla = bla + '<td id="' + nbGame + '-' + i + '" ><input id="input-'+ nbGame + '-' + i + '" class="form-control" type="number" /><input type="radio" name="pobeda" value="' + i + '" class="form-control check-pobeda" id="check-'+ nbGame + '-' + i + '" /></td>\n';
  }
  bla = bla + '</tr>';
  return bla;
}
